<?php

/**
 * @file
 * Defines the functions used for solr count.
 */

/**
 * Store the data in cache for each vocaubalry passed
 * @param type $index
 * @param type $extra_parameter
 * @param type $fetch
 * @param type $interval_hour
 * @return string
 */
function custom_solr_get_solr_data($index, $fetch = 0, $extra_parameter = array(), $interval_hour = 3, $free_text = '') {
  if ($index == '') {
    return '';
  }
  $search_fields = explode("\n", variable_get($index . '_textarea', 'spell'));
  $optional_parameter = custom_solr_process_extra_parameter($extra_parameter);
  $free_parameter = custom_solr_process_free_search_parameter($free_text);
  $query_field = custom_solr_process_query_fields_parameter($search_fields);
  $key = str_ireplace(' ', '_', $index . $optional_parameter . $free_parameter);
  $content = cache_get($key, SOLR_CACHE_TABLE);
  $reload_tree = custom_solr_get_tree_status($content->created, $interval_hour);
  if ($reload_tree == FALSE) { // If timeout has not happened check for data presence
    $data = $content->data;
    if (($fetch == 1) && ($content->data == 0)) {
      $reload_tree = TRUE;
    }
  }
  if ($reload_tree == TRUE) {
    $data = custom_solr_fetch_solr($index, $optional_parameter, $free_parameter, $query_field);
    cache_set($key, $data, SOLR_CACHE_TABLE);
  }
  return $data;
}

/**
 * Check the generation time in the cache
 * @param type $time_key
 * @param type $interval_hour
 * @return boolean
 */
function custom_solr_get_tree_status($last_execution_time, $interval_hour = 3) {
  $elapsed_time = '';
  $allowed_difference = 60 * 60 * $interval_hour;// Reload at each 3 hour
  $reload_tree = FALSE;
  if (empty($last_execution_time)) {
    $reload_tree = TRUE;
  }
  else {
    $elapsed_time = time() - $last_execution_time;
    if ($elapsed_time >= $allowed_difference) {
      $reload_tree = TRUE;
    }
  }
  return $reload_tree;
}

/**
 * Fetch data from Solr
 * @staticvar null $uri
 * @param type $index
 * @param type $extra_parameter
 * @param type $limit
 * @return int
 */
function custom_solr_fetch_solr($index, $optional_parameter, $free_parameter, $query_fields, $limit = 0) {
  static $uri = NULL;
  if (!isset($uri)) {
    $uri = custom_solr_get_solr_path();
  }
  if (empty($uri)) {
    return 0;
  }

  $params = array('fq' => '', 'fl' => '', 'q' => '', 'rows' => '0', 'start' => '0', 'wt' => 'json', 'json.nl' => 'map');
  if (!empty($free_parameter)) {
    $params['q'] = $free_parameter;
  }
  $params['fq'] = (!empty($index)) ? 'index_id' . '%3A' . $index : '';
  $params['fq'] = (!empty($optional_parameter)) ? $params['fq'] . '&' . $optional_parameter : $params['fq'];
  $params['rows'] = intval($limit);
  $data = $query_fields;
  foreach ($params as $param_key => $param_value) {
    $data = ($data != '') ? $data . '&' . $param_key . '=' . $param_value : $param_key . '=' . $param_value  ;
  }
  $url = $uri . $data;
  $options = array('method' => 'POST');
  $result = drupal_http_request($url, $options);
  if ($result->code == 200) {
    $response = json_decode($result->data);
    return (isset($response->response->numFound)) ? $response->response->numFound : 0;
  }
  return 0;
}

/**
 * Process Query fields
 * @param type $query_field
 * @return type
 */
function custom_solr_process_query_fields_parameter($query_field = array()) {
  $output_parameter = '';
  if (is_array($query_field)) {
    foreach ($query_field as $pos => $value) {
      $value = trim($value);
      if (empty($value)) {
        continue;
      }
      $value = str_ireplace(":", "%3A", $value);
      $value = str_ireplace("/", "%2F", $value);
      $value = str_ireplace(" ", "%2B", $value);
      if (empty($value)) {
        continue;
      }
      $output_parameter = (!empty($output_parameter)) ? $output_parameter . '&qf=' . $value : 'qf=' . $value;
    }
  }
  return $output_parameter;
}

/**
 * Process the optional parameter for solr search
 * @param type $extra_parameter
 * @return type
 */
function custom_solr_process_free_search_parameter($free_text = array()) {
  $free_text = trim($free_text);
  $output_parameter = '';
  $parameter = explode(" ", $free_text);
  if (is_array($parameter)) {
    foreach ($parameter as $pos => $value) {
      $value = str_ireplace(":", "\%3A", $value);
      $value = str_ireplace("/", "%2F", $value);
      $value = str_ireplace(" ", "%2B", $value);
      if (empty($value)) {
        continue;
      }
      $output_parameter = (!empty($output_parameter)) ? $output_parameter . '+' . '%22' . $value . '%22' : '%22' . $value . '%22';
    }
  }
  return $output_parameter;
}

/**
 * Process the optional parameter for solr search
 * @param type $extra_parameter
 * @return type
 */
function custom_solr_process_extra_parameter($extra_parameter = array()) {
  $parameter = '';
  if (is_array($extra_parameter)) {
    foreach ($extra_parameter as $key => $value) {
      $key = str_ireplace(":", "\%3A", $key);
      $value = str_ireplace(":", "\%3A", $value);
      $value = str_ireplace("/", "%2F", $value);
      $value = str_ireplace(" ", "%2B", $value);
      if (empty($value)) {
        continue;
      }
      $parameter = (!empty($parameter)) ? $parameter . 'fq=' . $key . '%3A' . $value : "fq=" . $key . '%3A' . $value;
    }
  }
  return $parameter;
}

/**
 * Get solr path
 * @return string
 */
function custom_solr_get_solr_path() {
  $uri = '';
  $options = db_query('SELECT options FROM {search_api_server} WHERE enabled = :enabled and status = :status',
  array(':enabled' => 1, ':status' => 1))->fetchField();
  if (!empty($options)) {
    $solr = unserialize($options);
    $http  = (!valid_url($solr['host'], TRUE)) ? 'http://' . $solr['host'] : $solr['host'];
    $uri = $http . ':' . $solr['port'] . $solr['path'] . '/select?';
  }
  return $uri;
}
